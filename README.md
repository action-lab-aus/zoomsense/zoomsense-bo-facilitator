# ZoomSense - Breakout Room Facilitator

This system is aimed at facilitating the control of Zoom breakout rooms (using the Zoom official Android SDK), especially for large meetings where assigning/switching users to breakout rooms requires a huge amount of manual effort.

# Getting Started

## 1. Create a Zoom Email/Password Account

The Breakout Room Facilitator requires the host to join a specific Zoom meeting using both a Desktop client and an Android client (using the same email/password credentials). These two logged-in clients will have the same privilege and control over the meeting, and they will be able to reclaim and assign host permissions to each other mutually.

![Reclaim Host](img/ReclaimHost.gif)

## 2. Generate **Zoom SDK** Key and Secret

Under the **Develop** tab, select **Build App**. Choose SDK to start creating an SDK App on Zoom. Provide basic information and developer details under the **Information** section including:

- Company Name
- Developer Contact Name
- Developer Contact Email Address

Under the **App Credentials** section, you will be able to find the SDK Key & Secret for developing the ZoomSensor application. Add the App Key and Secret under `InitAuth/AuthConstants.java`.

## 3. Create a Firebase Project

- Enable `Realtime Database` for the Firebase Project
- Add Firebase to the Android Project. For more information, please check [Setup Firebase Project for Android](https://firebase.google.com/docs/android/setup).

## 4. Download and Integrate the Zoom Android SDK

Download the lasted Zoom Android SDK from [Zoom SDK Android](https://github.com/zoom/zoom-sdk-android/archive/master.zip) and follow the instructions to [Integrate the Zoom SDK](https://marketplace.zoom.us/docs/sdk/native-sdks/android/getting-started/integration).

# Current Features

## 1. Enabled Zoom Custom UI

Breakout Room features are not supported in the official Android Zoom client. Hence, Zoom Custom UI is used to enable the control for breakout rooms. The current supported Zoom Custom UI features are limited to:

- Audio Control (connect audio, mute/unmute audio)
- Video Control (display/hide video)
- Chat Control (send and receive chat messages)
- Reclaim Host Privilege
- Breakout Room Control (add breakout rooms, start/close bo sessions)

## 2. Create and Assign Users to Breakout Rooms

Assume the Zoom meeting participants will go through a registration process for recording their preferences for breakout rooms, the Breakout Room Facilitator will be able to pull the session configurations from Firebase RTDB and create breakout rooms dynamically. Participants will also be assigned to the breakout rooms accordingly so that the host/co-hosts will not need to assign them manually.

![Create Breakout Rooms from Firebase](img/CreateBoFromFirebase.gif)

## 3. Real-time switching for participants joining a different breakout room

Meeting attendees will be able to influence the control of the breakout room by self-selecting a breakout room based on certain criteria. The current implementation listens to the `onChildAdded` events for the breakout room nodes and moves participants according to the changes. However, this could be handled via a chat conversation with the integration of [ZoomSense Windows Sensors](https://gitlab.com/action-lab-aus/zoomsense/zoomsense).

![Realtime Switching of BO Participants](img/RealtimeAssignUsersToBo.gif)

# Future Roadmap

The current roadmap includes:

- Design and develop the custom registration system for forming the breakout rooms (either on-demand or pre-allocation can be achieved)
- Provide the UI to share the control of breakout rooms with multiple meeting administrators
- Enable participant-to-bot interaction for switching to other running breakout rooms dynamically
- Facilitate the coordination between the meeting admins who are inside breakout rooms with "proxied" messages via ZoomSensors

# Limitations

Through the Android SDK, we are currently not able to retrieve the unique identifier for each meeting participant. Hence, the mapping of the registrants and actual meeting participants is through the Zoom meeting display name.

In that regard, it is recommended to disable users to rename themselves during the meeting. This can be achieved via:

1. Before scheduling meetings, navigate to _Settings > In Meeting (Basic) > Uncheck Allow Participants to Rename Themselves_, or
2. During the meeting, click _More > Uncheck Allow Participants to Rename Themselves_ through the meeting option bar by the host.
