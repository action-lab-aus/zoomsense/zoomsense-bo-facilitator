package us.zoom.zoomsense.JoinMeeting;

import android.content.Context;

import us.zoom.sdk.JoinMeetingOptions;
import us.zoom.sdk.JoinMeetingParams;
import us.zoom.sdk.MeetingService;
import us.zoom.sdk.ZoomSDK;

public class JoinMeetingHelper {
    private ZoomSDK mZoomSDK;

    private final static String DISPLAY_NAME = "ZoomSense Helper";

    private JoinMeetingHelper() {
        mZoomSDK = ZoomSDK.getInstance();
    }

    public synchronized static JoinMeetingHelper getInstance() {
        JoinMeetingHelper mJoinMeetingHelper = new JoinMeetingHelper();
        return mJoinMeetingHelper;
    }

    public int joinMeetingWithNumber(Context context, String meetingNo, String meetingPassword) {
        MeetingService meetingService = mZoomSDK.getMeetingService();
        if (meetingService == null)
            return -1;

        JoinMeetingOptions opts = new JoinMeetingOptions();
        JoinMeetingParams params = new JoinMeetingParams();
        params.displayName = DISPLAY_NAME;
        params.meetingNo = meetingNo;
        params.password = meetingPassword;

        return meetingService.joinMeetingWithParams(context, params,opts);
    }
}
