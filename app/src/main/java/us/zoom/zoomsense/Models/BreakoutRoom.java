package us.zoom.zoomsense.Models;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BreakoutRoom {
    private long startTime;
    private HashMap<String, List<String>> boParticipants;

    public BreakoutRoom() {

    }

    public BreakoutRoom(long startTime, HashMap<String, List<String>> boParticipants) {
        this.startTime = startTime;
        this.boParticipants = boParticipants;
    }

    public long getStartTime() {
        return startTime;
    }

    public HashMap<String, List<String>> getBoParticipants() {
        return boParticipants;
    }

    public String getBoDetails() {
        String boDetails = "";
        for(Map.Entry<String, List<String>> entry: boParticipants.entrySet()) {
            String boName = entry.getKey();
            List<String> boUsers = entry.getValue();
            String userNameList = "";
            for (int i = 0; i < boUsers.size(); i++)
                userNameList += boUsers.get(i) + ", ";
            boDetails += boName + ": " + userNameList + "; ";
        }
        return boDetails;
    }
}
