package us.zoom.zoomsense.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;

import us.zoom.sdk.InMeetingNotificationHandle;
import us.zoom.sdk.MeetingServiceListener;
import us.zoom.sdk.MeetingSettingsHelper;
import us.zoom.sdk.MeetingStatus;
import us.zoom.sdk.ZoomApiError;
import us.zoom.sdk.ZoomAuthenticationError;
import us.zoom.sdk.ZoomError;
import us.zoom.sdk.ZoomSDK;
import us.zoom.zoomsense.R;
import us.zoom.zoomsense.InitAuth.InitAuthSDKCallback;
import us.zoom.zoomsense.InitAuth.InitAuthSDKHelper;
import us.zoom.zoomsense.InMeeting.CustomUI.MyMeetingActivity;
import us.zoom.zoomsense.InMeeting.CustomUI.View.MeetingWindowHelper;
import us.zoom.zoomsense.JoinMeeting.UserLoginCallback;

public class InitAuthSDKActivity extends Activity implements InitAuthSDKCallback,
        MeetingServiceListener, UserLoginCallback.ZoomAuthListener {

    private final static String TAG = "InitAuthSDKActivity";
    private ZoomSDK mZoomSDK;
    private boolean isResumed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mZoomSDK = ZoomSDK.getInstance();
        if (mZoomSDK.isLoggedIn()) {
            finish();
            showEmailLoginUserStartJoinActivity();
            return;
        }

        setContentView(R.layout.init_auth_sdk);

        InitAuthSDKHelper.getInstance().initSDK(this, this);

        if (mZoomSDK.isInitialized())
            mZoomSDK.getMeetingService().addListener(this);
    }

    InMeetingNotificationHandle handle = new InMeetingNotificationHandle() {
        @Override
        public boolean handleReturnToConfNotify(Context context, Intent intent) {
            intent = new Intent(context, MyMeetingActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            if(!(context instanceof Activity)) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            }
            intent.setAction(InMeetingNotificationHandle.ACTION_RETURN_TO_CONF);
            context.startActivity(intent);
            return true;
        }
    };

    @Override
    public void onZoomSDKInitializeResult(int errorCode, int internalErrorCode) {
        Log.i(TAG, "onZoomSDKInitializeResult, errorCode = " + errorCode + ", internalErrorCode = " + internalErrorCode);

        if (errorCode != ZoomError.ZOOM_ERROR_SUCCESS) {
            Toast.makeText(this, "Failed to initialize Zoom SDK. errorCode = " + errorCode + ", internalErrorCode = " + internalErrorCode, Toast.LENGTH_LONG).show();
        } else {
            MeetingSettingsHelper meetingSettingsHelper = mZoomSDK.getMeetingSettingsHelper();
            meetingSettingsHelper.enable720p(false);
            meetingSettingsHelper.enableShowMyMeetingElapseTime(true);
            meetingSettingsHelper.setCustomizedNotificationData(null, handle);
            meetingSettingsHelper.setCustomizedMeetingUIEnabled(true);

            mZoomSDK.getMeetingService().addListener(this);
            Toast.makeText(this, "Initialize Zoom SDK successfully!", Toast.LENGTH_LONG).show();
            showEmailLoginActivity();
            if (mZoomSDK.tryAutoLoginZoom() == ZoomApiError.ZOOM_API_ERROR_SUCCESS)
                UserLoginCallback.getInstance().addListener(this);
        }
    }

    @Override
    public void onZoomSDKLoginResult(long result) {
        if ((int) result == ZoomAuthenticationError.ZOOM_AUTH_ERROR_SUCCESS) {
            showEmailLoginUserStartJoinActivity();
            finish();
        }
    }

    @Override
    public void onZoomSDKLogoutResult(long result) {

    }

    @Override
    public void onZoomIdentityExpired() {
        if (mZoomSDK.isLoggedIn())
            mZoomSDK.logoutZoom();
    }

    @Override
    public void onZoomAuthIdentityExpired() {
        Log.e(TAG,"onZoomAuthIdentityExpired");
    }

    private void showEmailLoginActivity() {
        Intent intent = new Intent(this, EmailUserLoginActivity.class);
        startActivity(intent);
    }

    private void showEmailLoginUserStartJoinActivity() {
        Intent intent = new Intent(this, LoginUserStartJoinMeetingActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        isResumed = true;
        refreshUI();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isResumed = false;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if(mZoomSDK.getMeetingService() != null)
            mZoomSDK.getMeetingService().removeListener(this);
        UserLoginCallback.getInstance().removeListener(this);
        InitAuthSDKHelper.getInstance().reset();
    }

    private void refreshUI() {
        if(!ZoomSDK.getInstance().isInitialized())
            return;

        MeetingStatus meetingStatus = mZoomSDK.getMeetingService().getMeetingStatus();
        if (mZoomSDK.getMeetingSettingsHelper().isCustomizedMeetingUIEnabled()) {
            if (meetingStatus == MeetingStatus.MEETING_STATUS_INMEETING && isResumed)
                MeetingWindowHelper.getInstance().showMeetingWindow(this);
            else
                MeetingWindowHelper.getInstance().hiddenMeetingWindow(true);
        }
    }

    @Override
    public void onMeetingStatusChanged(MeetingStatus meetingStatus, int errorCode, int internalErrorCode) {
        Log.d(TAG,"onMeetingStatusChanged "+meetingStatus+":"+errorCode);
        if(!mZoomSDK.isInitialized())
            return;
        if (mZoomSDK.getMeetingSettingsHelper().isCustomizedMeetingUIEnabled()) {
            if (meetingStatus == MeetingStatus.MEETING_STATUS_CONNECTING) {
                showMeetingUI();
            }
        }
        refreshUI();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        MeetingWindowHelper.getInstance().onActivityResult(requestCode, this);
    }

    private void showMeetingUI() {
        if (mZoomSDK.getMeetingSettingsHelper().isCustomizedMeetingUIEnabled()) {
            Intent intent = new Intent(this, MyMeetingActivity.class);
            intent.putExtra("from", MyMeetingActivity.JOIN_FROM_UNLOGIN);
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            this.startActivity(intent);
        }
    }
}