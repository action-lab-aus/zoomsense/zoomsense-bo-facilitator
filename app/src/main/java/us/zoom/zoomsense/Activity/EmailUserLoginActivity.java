package us.zoom.zoomsense.Activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import us.zoom.sdk.ZoomApiError;
import us.zoom.sdk.ZoomAuthenticationError;
import us.zoom.zoomsense.R;
import us.zoom.zoomsense.JoinMeeting.UserLoginCallback;
import us.zoom.zoomsense.JoinMeeting.EmailUserLoginHelper;

public class EmailUserLoginActivity extends Activity implements UserLoginCallback.ZoomAuthListener, View.OnClickListener {
	private EditText mEdtUserName;
	private EditText mEdtPassword;
	private Button mBtnLogin;
	private View mProgressPanel;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.email_login_activity);
		
		mEdtUserName = findViewById(R.id.userName);
		mEdtPassword = findViewById(R.id.password);
		mBtnLogin = findViewById(R.id.btnLogin);
		mBtnLogin.setOnClickListener(this);
		mProgressPanel = findViewById(R.id.progressPanel);
	}
	
	@Override
	protected void onResume() {
		super.onResume();

		UserLoginCallback.getInstance().addListener(this);
	}

	@Override
	protected void onPause() {
		super.onPause();

		UserLoginCallback.getInstance().removeListener(this);
	}
	
	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.btnLogin) {
			onClickBtnLogin();
		}
	}
	
	private void onClickBtnLogin() {
		String userName = mEdtUserName.getText().toString().trim();
		String password = mEdtPassword.getText().toString().trim();

		if(userName.length() == 0 || password.length() == 0) {
			Toast.makeText(this, "Email and password are required...", Toast.LENGTH_LONG).show();
			return;
		}

		int ret = EmailUserLoginHelper.getInstance().login(userName, password);
		if(!(ret == ZoomApiError.ZOOM_API_ERROR_SUCCESS)) {
			if (ret == ZoomApiError.ZOOM_API_ERROR_EMAIL_LOGIN_IS_DISABLED)
				Toast.makeText(this, "Account has disable email login...", Toast.LENGTH_LONG).show();
			else
				Toast.makeText(this, "Zoom SDK has not been initialized successfully or SDK is logging in...", Toast.LENGTH_LONG).show();
		} else {
			mBtnLogin.setVisibility(View.GONE);
			mProgressPanel.setVisibility(View.VISIBLE);
		}
	}

	@Override
	public void onZoomSDKLoginResult(long result) {
		if(result == ZoomAuthenticationError.ZOOM_AUTH_ERROR_SUCCESS) {
			Toast.makeText(this, "Login successfully...", Toast.LENGTH_SHORT).show();
			Intent intent = new Intent(this, LoginUserStartJoinMeetingActivity.class);
			startActivity(intent);
			UserLoginCallback.getInstance().removeListener(this);
			finish();
		} else
			Toast.makeText(this, "Login failed, result code = " + result, Toast.LENGTH_SHORT).show();
		mBtnLogin.setVisibility(View.VISIBLE);
		mProgressPanel.setVisibility(View.GONE);
	}

	@Override
	public void onZoomSDKLogoutResult(long result) {
		if(result == ZoomAuthenticationError.ZOOM_AUTH_ERROR_SUCCESS)
			Toast.makeText(this, "Logout successfully...", Toast.LENGTH_SHORT).show();
		else
			Toast.makeText(this, "Logout failed, result code = " + result, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onZoomIdentityExpired() {

	}

	@Override
	public void onZoomAuthIdentityExpired() {

	}
}
