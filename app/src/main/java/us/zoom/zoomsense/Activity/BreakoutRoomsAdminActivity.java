package us.zoom.zoomsense.Activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import us.zoom.androidlib.app.ZMActivity;
import us.zoom.sdk.IBOAdmin;
import us.zoom.sdk.IBOAssistant;
import us.zoom.sdk.IBOAttendee;
import us.zoom.sdk.IBOCreator;
import us.zoom.sdk.IBOData;
import us.zoom.sdk.IBODataEvent;
import us.zoom.sdk.IBOMeeting;

import us.zoom.sdk.InMeetingBOController;
import us.zoom.sdk.InMeetingBOControllerListener;
import us.zoom.sdk.InMeetingChatController;
import us.zoom.sdk.ZoomSDK;
import us.zoom.zoomsense.R;
import us.zoom.zoomsense.Activity.Dialog.AssignNewUserToRunningBODialog;
import us.zoom.zoomsense.Activity.Dialog.AssignUserToBODialog;
import us.zoom.zoomsense.Models.BreakoutRoom;

public class BreakoutRoomsAdminActivity extends ZMActivity implements InMeetingBOControllerListener {
    public static final int REQUEST_ID_BO_EDIT = 1;
    private final static String TAG = BreakoutRoomsAdminActivity.class.getSimpleName();
    private final static String BO_TAG = "BreakoutRoom Listener";
    private final static String RTDB_TAG = "Firebase RTDB";

    private FirebaseDatabase mDatabase;
    private DatabaseReference mBoRef;
    private HashMap<DatabaseReference, ChildEventListener> mBoListeners = new HashMap<>();
    private List<String> mBoRefPath = new ArrayList<>();
    private String mBoRefString;

    InMeetingBOController mBoController;
    InMeetingChatController mChatController;
    ListView mBoLv;
    ListView mUnassignedUsersLv;
    Button mBtnOpenBo;
    Button mBtnAddBo;
    BoListAdapter mAdapter;
    BoNewUserListAdapter mNewUsersAdapter;
    int mBoCount = 0;

    List<IBOMeeting> mList = new ArrayList<>();
    List<String> mNewUserList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_bo_admin);
        mBoLv = findViewById(R.id.lv_bo);
        mUnassignedUsersLv = findViewById(R.id.lv_unassigned_users);
        mBtnOpenBo = findViewById(R.id.btn_open_bo);
        mBtnAddBo = findViewById(R.id.btn_add_bo);

        mBoController = ZoomSDK.getInstance().getInMeetingService().getInMeetingBOController();
        mChatController = ZoomSDK.getInstance().getInMeetingService().getInMeetingChatController();
        mBoController.addListener(this);
        mAdapter = new BoListAdapter(this, mList, mBoController);
        mNewUsersAdapter = new BoNewUserListAdapter(this, mNewUserList, mBoController);

        mDatabase = FirebaseDatabase.getInstance("https://zoomsensedev-bo.firebaseio.com/");
        if (null != getIntent().getExtras()) {
            mBoRefString = getIntent().getExtras().getString("boRefString");
            mBoRef = mDatabase.getReference(mBoRefString);
        }

        // Load the breakout room ids when constructing the activity
        IBOData iboData = mBoController.getBODataHelper();
        if (iboData != null) {
            List<String> bIds = iboData.getBOMeetingIDList();
            if (bIds != null && bIds.size() != 0) {
                mBoCount = bIds.size();
                for (String id : bIds) {
                    IBOMeeting iboMeeting=iboData.getBOMeetingByID(id);
                    if(null!=iboMeeting)
                    {
                        mList.add(iboMeeting);
                    }
                }
            }
        }

        mBoLv.setAdapter(mAdapter);
        mUnassignedUsersLv.setAdapter(mNewUsersAdapter);

        mBoLv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
                Intent intent = new Intent(BreakoutRoomsAdminActivity.this, BoEditActivity.class);
                intent.putExtra(BoEditActivity.ARG_BO_ID, mList.get(position).getBoId());
                startActivityForResult(intent, REQUEST_ID_BO_EDIT);
            }
        });

        boolean isBOStarted = mBoController.isBOStarted();
        mBtnOpenBo.setText(isBOStarted ? "Close All BO" : "Open All BO");
        mBtnAddBo.setEnabled(!isBOStarted);
    }

    @Override
    protected void onResume() {
        super.onResume();
        registBoDataEvent();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unRegistBoDataEvent();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mBoController.removeListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_ID_BO_EDIT:
                IBOData iboData = mBoController.getBODataHelper();
                if (iboData != null) {
                    mList.clear();
                    List<String> bIds = iboData.getBOMeetingIDList();
                    if (bIds != null && bIds.size() > 0) {
                        mBoCount = bIds.size();
                        for (String id : bIds) {
                            IBOMeeting iboMeeting = iboData.getBOMeetingByID(id);
                            if (null != iboMeeting)
                                mList.add(iboMeeting);
                        }
                    }
                    mAdapter.notifyDataSetChanged();
                    mBtnOpenBo.setEnabled(mList.size() > 0);
                }
                break;
        }
    }

    private String createBO(String boName) {
        IBOCreator iboCreator = mBoController.getBOCreatorHelper();
        String bId = "";
        if (iboCreator != null) {
            mBoCount++;
            if (boName.length() != 0)
                bId = iboCreator.createBO(boName);
            else
                bId = iboCreator.createBO("BO_" + mBoCount);

            IBOData iboData = mBoController.getBODataHelper();
            if (iboData != null) {
                IBOMeeting iboMeeting = iboData.getBOMeetingByID(bId);
                if (null != iboMeeting)
                    mList.add(iboMeeting);
            }
        }

        mAdapter.notifyDataSetChanged();
        mBtnOpenBo.setEnabled(mList.size() > 0);
        return bId;
    }

    public void onClose(View view) {
        finish();
    }

    /**
     * When the add bo button is clicked, read the firebase node and construct the BreakoutRoom object.
     * Create and assign users to breakout rooms based on the matching of their display name
     * @param view
     */
    public void onClickAddBO(View view) {
        mBoRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                mBoRefPath.clear();
                BreakoutRoom breakRoom = dataSnapshot.getValue(BreakoutRoom.class);
                HashMap<String, List<String>> boParticipants = breakRoom.getBoParticipants();

                // Return if no breakout room participant information has been recorded
                if (boParticipants == null) {
                    Toast.makeText(BreakoutRoomsAdminActivity.this, "No participant registration information...", Toast.LENGTH_SHORT).show();
                    return;
                }

                IBOData iboData = mBoController.getBODataHelper();
                List<String> unassignedList = iboData == null ? null : iboData.getUnassginedUserList();
                HashMap<String, String> unassignedUserMap = mapUidAndName(unassignedList);
                Log.i(RTDB_TAG, "Unassigned User Names: " + unassignedUserMap.toString());
                Log.i(RTDB_TAG, "BO Participants: " + boParticipants);

                IBOCreator iboCreator = mBoController.getBOCreatorHelper();
                for (Map.Entry<String, List<String>> entry: boParticipants.entrySet()) {
                    String boName = entry.getKey();
                    mBoRefPath.add(mBoRefString + "/boParticipants/" + boName);
                    int retry = 0;
                    String bId = "";
                    while (retry < 10) {
                        bId = createBO(boName);
                        if (bId.length() != 0) break;
                        else {
                            retry++;
                            try {
                                Thread.sleep(500);
                                Log.i(RTDB_TAG, "Failed to create bo " + boName + ", retry = " + retry);
                            } catch (Exception e) {

                            }
                        }
                    }

                    Log.i(RTDB_TAG, "BO ID: " + bId);
                    List<String> boUsers = entry.getValue();
                    for (int i = 0; i < boUsers.size(); i++) {
                        Log.i(RTDB_TAG, "BO User Name: " + boUsers.get(i));
                        if (unassignedUserMap.containsKey(boUsers.get(i)))
                            iboCreator.assignUserToBO(unassignedUserMap.get(boUsers.get(i)), bId);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    /**
     * Start or stop the breakout room session if the button is clicked.
     * Register and unregister bo listener for monitoring the participant allocation.
     * @param view
     */
    public void onClickStartBO(View view) {
        IBOAdmin iboAdmin = mBoController.getBOAdminHelper();
        if (iboAdmin != null) {
            if (mBoController.isBOStarted()) {
                if (iboAdmin.stopBO()) {
                    removeBoRefListener();
                    mBtnOpenBo.setText("Open All BO");
                    mBtnAddBo.setEnabled(true);
                    mAdapter.notifyDataSetChanged();
                }
            } else {
                if (iboAdmin.startBO()) {
                    addBoRefListener();
                    mBtnOpenBo.setText("Close All BO");
                    mBtnAddBo.setEnabled(false);
                    mAdapter.notifyDataSetChanged();
                }
            }
            refreshUnassignedNewUsersList();
        }
    }

    private void addBoRefListener() {
        for (int i = 0; i < mBoRefPath.size(); i++) {
            Log.i(RTDB_TAG, "addBoRefListener mBoRefPath: " + mBoRefPath.get(i));
            ChildEventListener boListener = new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot snapshot, String previousChildName) {
                    Log.i(RTDB_TAG, "onChildAdded Snapshot: " + snapshot.toString());

                    // Map uid and display name in all breakout room sessions
                    List<String> userList = new ArrayList<>();
                    for (int i = 0; i < mList.size(); i++)
                        userList.addAll(mList.get(i).getBoUserList());
                    HashMap<String, String> userMap = mapUidAndName(userList);

                    // Check whether the new name exists in the meeting
                    String userName = snapshot.getValue().toString();
                    if (userMap.containsKey(userName)) {
                        IBOAdmin iboAdmin = mBoController.getBOAdminHelper();
                        // Get breakout room id to be assigned
                        String iboName = snapshot.getRef().getParent().getKey();
                        String iboId = getBoIdByName(iboName);
                        Log.i(RTDB_TAG, "iboId: " + iboId);
                        Log.i(RTDB_TAG, "uid: " + userMap.get(userName));
                        if (iboId != null)
                            iboAdmin.switchAssignedUserToRunningBO(userMap.get(userName), iboId);
                        else
                            Toast.makeText(BreakoutRoomsAdminActivity.this, "Invalid database entry...", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onChildChanged(DataSnapshot snapshot, String previousChildName) {
                    Log.i(RTDB_TAG, "onChildChanged Snapshot: " + snapshot.toString());
                }

                @Override
                public void onChildRemoved(DataSnapshot snapshot) {
                    Log.i(RTDB_TAG, "onChildRemoved Snapshot: " + snapshot.toString());
                }

                @Override
                public void onChildMoved(DataSnapshot snapshot, String previousChildName) {
                    Log.i(RTDB_TAG, "onChildMoved Snapshot: " + snapshot.toString());
                }

                @Override
                public void onCancelled(DatabaseError error) {

                }
            };

            DatabaseReference boRef = mDatabase.getReference(mBoRefPath.get(i));
            boRef.addChildEventListener(boListener);
            mBoListeners.put(boRef, boListener);
            Log.i(RTDB_TAG, "addBoRefListener mBoListeners: " + mBoListeners.toString());
        }
    }

    private void removeBoRefListener() {
        for (Map.Entry<DatabaseReference, ChildEventListener> entry: mBoListeners.entrySet()) {
            DatabaseReference boRef = entry.getKey();
            ChildEventListener boListener = entry.getValue();
            boRef.removeEventListener(boListener);
        }
        mBoListeners.clear();
    }

    /**
     * Map Zoom String UID with Display Name
     *
     * @param userList List of String uids to be mapped
     * @return HashMap for UID: DisplayName pairs
     */
    private HashMap<String, String> mapUidAndName(List<String> userList) {
        IBOData iboData = mBoController.getBODataHelper();
        HashMap<String, String> userMap = new HashMap<>();
        for (int i  = 0; i < userList.size(); i++) {
            String uid = userList.get(i);
            String displayName = iboData.getBOUserName(uid);
            userMap.put(displayName, uid);
        }
        return userMap;
    }

    /**
     * Get the breakout room String id by the breakout room name
     *
     * @param boName Breakout room name
     * @return Breakout room string id
     */
    private String getBoIdByName(String boName) {
        for (int i = 0; i < mList.size(); i++) {
            IBOMeeting iboMeeting = mList.get(i);
            if (iboMeeting.getBoName().equals(boName)) return iboMeeting.getBoId();
        }
        return null;
    }

    private void refreshUnassignedNewUsersList() {
        mNewUserList.clear();
        if (!mBoController.isBOStarted()) {
            mUnassignedUsersLv.setVisibility(View.GONE);
            mNewUsersAdapter.notifyDataSetChanged();
            return;
        }

        IBOData iboData = mBoController.getBODataHelper();
        List<String> list = iboData == null ? null : iboData.getUnassginedUserList();
        if (list != null && list.size() > 0) {
            mUnassignedUsersLv.setVisibility(View.VISIBLE);
            mNewUserList.addAll(list);
        } else
            mUnassignedUsersLv.setVisibility(View.GONE);
        mNewUsersAdapter.notifyDataSetChanged();
    }

    @Override
    public void onHasCreatorRightsNotification(IBOCreator iboCreator) {
        Log.d(BO_TAG, "onHasCreatorRightsNotification");
        mList.clear();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onHasAdminRightsNotification(IBOAdmin iboAdmin) {
        Log.d(TAG, "onHasAdminRightsNotification");
    }

    @Override
    public void onHasAssistantRightsNotification(IBOAssistant iboAssistant) {
        Log.d(TAG, "onHasAssistantRightsNotification");
    }

    @Override
    public void onHasAttendeeRightsNotification(IBOAttendee iboAttendee) {
        Log.d(TAG, "onHasAttendeeRightsNotification");
    }

    @Override
    public void onHasDataHelperRightsNotification(IBOData iboData) {
        Log.d(TAG, "onHasDataHelperRightsNotification");
    }

    @Override
    public void onLostCreatorRightsNotification() {
        Log.d(TAG, "onLostCreatorRightsNotification");
    }

    @Override
    public void onLostAdminRightsNotification() {
        Log.d(TAG, "onLostAdminRightsNotification");
    }

    @Override
    public void onLostAssistantRightsNotification() {
        Log.d(TAG, "onLostAssistantRightsNotification");
    }

    @Override
    public void onLostAttendeeRightsNotification() {
        Log.d(TAG, "onLostAttendeeRightsNotification");
    }

    @Override
    public void onLostDataHelperRightsNotification() {
        Log.d(TAG, "onLostDataHelperRightsNotification");
    }

    public static class BoListAdapter extends BaseAdapter {
        private Context context;
        private List<IBOMeeting> list;
        private InMeetingBOController boController;

        public BoListAdapter(@NonNull Context context, @NonNull List<IBOMeeting> list, InMeetingBOController boController) {
            this.context = context;
            this.list = list;
            this.boController = boController;
        }

        @Override
        public int getCount() {
            return list == null ? 0 : list.size();
        }

        @Override
        @Nullable
        public Object getItem(int position) {
            if (position >= 0)
                return list.get(position);
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final String tag = "boListItem";
            View view;
            if (convertView != null && tag.equals(convertView.getTag()))
                view = convertView;
            else {
                view = LayoutInflater.from(context).inflate(R.layout.bo_list_item, parent, false);
                view.setTag(tag);
            }

            final IBOMeeting boMeeting = list.get(position);
            TextView tv_bo_name = view.findViewById(R.id.tv_bo_name);
            Button btn_assign = view.findViewById(R.id.btn_assign);
            tv_bo_name.setText(boMeeting.getBoName());

            if (boController.isBOStarted())
                btn_assign.setText("Join");
            else
                btn_assign.setText("Assign");

            btn_assign.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (boController.isBOStarted()) {
                        IBOAssistant iboAssistant = boController.getBOAssistantHelper();
                        if (iboAssistant != null) {
                            boolean success = iboAssistant.joinBO(boMeeting.getBoId());
                            Toast.makeText(context, success ? "Join successfully" : "Join failed", Toast.LENGTH_SHORT).show();
                            if (success) {
                                if (context instanceof BreakoutRoomsAdminActivity)
                                    ((BreakoutRoomsAdminActivity)context).finish();
                            }
                        }
                    } else {
                        IBOData iboData = boController.getBODataHelper();
                        if (iboData != null) {
                            List<String> unassignedUsers = iboData.getUnassginedUserList();
                            if (unassignedUsers == null || unassignedUsers.size() == 0) {
                                Toast.makeText(context, "All participants have been assigned to Breakout Rooms.", Toast.LENGTH_SHORT).show();
                            } else {
                                if (context instanceof ZMActivity) {
                                    FragmentManager fm = ((ZMActivity) context).getSupportFragmentManager();
                                    AssignUserToBODialog.show(fm, boMeeting.getBoId(), null);
                                }
                            }
                        }
                    }
                }
            });
            return view;
        }
    }

    public static class BoNewUserListAdapter extends BaseAdapter {
        private Context context;
        private List<String> list;
        private InMeetingBOController boController;

        public BoNewUserListAdapter(@NonNull Context context, @NonNull List<String> list, InMeetingBOController boController) {
            this.context = context;
            this.list = list;
            this.boController = boController;
        }

        @Override
        public int getCount() {
            return list == null ? 0 : list.size();
        }

        @Override
        @Nullable
        public Object getItem(int position) {
            if (position >= 0)
                return list.get(position);
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            final String tag = "boNewUserListItem";
            View view;
            if (convertView != null && tag.equals(convertView.getTag()))
                view = convertView;
            else {
                view = LayoutInflater.from(context).inflate(R.layout.bo_new_user_list_item, parent, false);
                view.setTag(tag);
            }

            TextView tv_user_name = view.findViewById(R.id.tv_user_name);
            Button btn_assign_to = view.findViewById(R.id.btn_assign_to);

            IBOData iboData = boController.getBODataHelper();
            if (iboData != null)
                tv_user_name.setText(iboData.getBOUserName(list.get(position)));
            btn_assign_to.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(context instanceof ZMActivity) {
                        FragmentManager fm = ((ZMActivity)context).getSupportFragmentManager();
                        AssignNewUserToRunningBODialog.show(fm, list.get(position), null);
                    }
                }
            });
            return view;
        }
    }

    private void registBoDataEvent(){
        IBOData iboData = mBoController.getBODataHelper();
        if(iboData != null) {
            iboData.setEvent(iboDataEvent);
        }
    }

    private void unRegistBoDataEvent(){
        IBOData iboData = mBoController.getBODataHelper();
        if(iboData != null) {
            iboData.setEvent(null);
        }
    }

    private IBODataEvent iboDataEvent = new IBODataEvent() {
        @Override
        public void onBOInfoUpdated(String strBOID) {
            refreshUnassignedNewUsersList();
        }

        @Override
        public void onUnAssignedUserUpdated() {
            refreshUnassignedNewUsersList();
        }
    };
}
