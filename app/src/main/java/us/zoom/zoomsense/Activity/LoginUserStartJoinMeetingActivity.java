package us.zoom.zoomsense.Activity;

import us.zoom.sdk.MeetingError;
import us.zoom.sdk.MeetingService;
import us.zoom.sdk.MeetingServiceListener;
import us.zoom.sdk.MeetingStatus;
import us.zoom.sdk.ZoomAuthenticationError;
import us.zoom.sdk.ZoomSDK;
import us.zoom.sdk.ZoomSDKAuthenticationListener;
import us.zoom.zoomsense.InitAuth.AuthConstants;
import us.zoom.zoomsense.R;
import us.zoom.zoomsense.InMeeting.CustomUI.MyMeetingActivity;
import us.zoom.zoomsense.InMeeting.CustomUI.View.MeetingWindowHelper;
import us.zoom.zoomsense.JoinMeeting.JoinMeetingHelper;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginUserStartJoinMeetingActivity extends Activity implements AuthConstants
        , MeetingServiceListener, ZoomSDKAuthenticationListener {
    private final static String TAG = "StartJoinMeeting";

    private EditText mEdtMeetingNo;
    private EditText mEdtMeetingPassword;

    private boolean mbPendingStartMeeting = false;
    private boolean isResumed = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login_user_start_join);

        mEdtMeetingNo = findViewById(R.id.edtMeetingNo);
        mEdtMeetingPassword = findViewById(R.id.edtMeetingPassword);

        registerListener();
    }

    private void registerListener() {
        ZoomSDK zoomSDK = ZoomSDK.getInstance();
        zoomSDK.addAuthenticationListener(this);

        MeetingService meetingService = zoomSDK.getMeetingService();
        if (meetingService != null)
            meetingService.addListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        isResumed = true;
        refreshUI();
    }

    @Override
    protected void onPause() {
        super.onPause();

        isResumed = false;
    }

    @Override
    protected void onDestroy() {
        ZoomSDK zoomSDK = ZoomSDK.getInstance();
        zoomSDK.removeAuthenticationListener(this);

        if (zoomSDK.isInitialized()) {
            MeetingService meetingService = zoomSDK.getMeetingService();
            meetingService.removeListener(this);
        }
        MeetingWindowHelper.getInstance().removeOverlayListener();

        super.onDestroy();
    }

    public void onClickBtnJoinMeeting(View view) {
        String meetingNo = mEdtMeetingNo.getText().toString().trim();
        String meetingPassword = mEdtMeetingPassword.getText().toString().trim();

        if (meetingNo.length() == 0) {
            Toast.makeText(this, "Meeting ID is required...", Toast.LENGTH_LONG).show();
            return;
        }

        ZoomSDK zoomSDK = ZoomSDK.getInstance();
        if (!zoomSDK.isInitialized()) {
            Toast.makeText(this, "Zoom SDK has not been initialized successfully...", Toast.LENGTH_LONG).show();
            return;
        }

        int ret = JoinMeetingHelper.getInstance().joinMeetingWithNumber(this, meetingNo, meetingPassword);
        Log.i(TAG, "onClickBtnJoinMeeting, ret = " + ret);
    }

    public void onClickBtnLogout(View view) {
        ZoomSDK zoomSDK = ZoomSDK.getInstance();
        if (!zoomSDK.logoutZoom()) {
            Toast.makeText(this, "Zoom SDK has not been initialized successfully...", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onMeetingStatusChanged(MeetingStatus meetingStatus, int errorCode, int internalErrorCode) {
        Log.i(TAG, "onMeetingStatusChanged, meetingStatus = " + meetingStatus
                + ", errorCode = " + errorCode + ", internalErrorCode = " + internalErrorCode);

        if (meetingStatus == MeetingStatus.MEETING_STATUS_FAILED && errorCode == MeetingError.MEETING_ERROR_CLIENT_INCOMPATIBLE)
            Toast.makeText(this, "Version of ZoomSDK is too low...", Toast.LENGTH_LONG).show();

        if (mbPendingStartMeeting && meetingStatus == MeetingStatus.MEETING_STATUS_IDLE)
            mbPendingStartMeeting = false;

        if (meetingStatus == MeetingStatus.MEETING_STATUS_CONNECTING)
            showMeetingUI();

        refreshUI();
    }

    private void refreshUI() {
        if(ZoomSDK.getInstance().getMeetingService() == null)
            return;

        MeetingStatus meetingStatus = ZoomSDK.getInstance().getMeetingService().getMeetingStatus();
        if (ZoomSDK.getInstance().getMeetingSettingsHelper().isCustomizedMeetingUIEnabled()) {
            if (meetingStatus == MeetingStatus.MEETING_STATUS_INMEETING && isResumed)
                MeetingWindowHelper.getInstance().showMeetingWindow(this);
            else
                MeetingWindowHelper.getInstance().hiddenMeetingWindow(true);
        }
    }

    private void showMeetingUI() {
        if (ZoomSDK.getInstance().getMeetingSettingsHelper().isCustomizedMeetingUIEnabled()) {
            Intent intent = new Intent(this, MyMeetingActivity.class);
            intent.putExtra("from", MyMeetingActivity.JOIN_FROM_LOGIN);
            intent.putExtra("meetingId", mEdtMeetingNo.getText().toString().trim());
            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            this.startActivity(intent);
        }
    }

    @Override
    public void onBackPressed() {
        if (ZoomSDK.getInstance().isLoggedIn())
            moveTaskToBack(true);
        else
            super.onBackPressed();
    }

    @Override
    public void onZoomSDKLoginResult(long l) {

    }

    @Override
    public void onZoomSDKLogoutResult(long result) {
        if (result == ZoomAuthenticationError.ZOOM_AUTH_ERROR_SUCCESS) {
            Toast.makeText(this, "Logout successfully...", Toast.LENGTH_SHORT).show();
            showLoginView();
        } else
            Toast.makeText(this, "Logout failed, result code = " + result, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onZoomIdentityExpired() {
        ZoomSDK.getInstance().logoutZoom();
    }

    @Override
    public void onZoomAuthIdentityExpired() {

    }

    private void showLoginView() {
        mEdtMeetingPassword.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (ZoomSDK.getInstance().isInitialized()) {
                    Intent intent = new Intent(LoginUserStartJoinMeetingActivity.this, EmailUserLoginActivity.class);
                    startActivity(intent);
                }
                finish();
            }
        },500);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        MeetingWindowHelper.getInstance().onActivityResult(requestCode,this);
    }
}
