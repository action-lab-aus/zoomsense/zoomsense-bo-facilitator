package us.zoom.zoomsense.InMeeting.CustomUI.View;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import us.zoom.sdk.InMeetingAudioController;
import us.zoom.sdk.InMeetingBOController;
import us.zoom.sdk.InMeetingService;
import us.zoom.sdk.InMeetingUserInfo;
import us.zoom.sdk.InMeetingVideoController;
import us.zoom.sdk.ZoomSDK;
import us.zoom.zoomsense.R;
import us.zoom.zoomsense.InMeeting.CustomUI.View.Adapter.SimpleMenuAdapter;
import us.zoom.zoomsense.InMeeting.CustomUI.View.Adapter.SimpleMenuItem;

public class MeetingOptionBar extends FrameLayout implements View.OnClickListener {
    private final int MENU_DISCONNECT_AUDIO = 0;
    private final int MENU_SHOW_PLIST = 4;
    private final int MENU_SPEAKER_ON = 9;
    private final int MENU_SPEAKER_OFF = 10;
    private final int MENU_CREATE_BO = 15;
    private final int MENU_RECLAIM_HOST = 17;
    MeetingOptionBarCallBack mCallBack;

    View mContentView;
    View mBottomBar;
    View mTopBar;

    private View mBtnLeave;
    private View mBtnCamera;
    private View mBtnAudio;
    private View mBtnSwitchCamera;
    private ImageView mAudioStatusImg;
    private ImageView mVideoStatusImg;
    private TextView mMeetingNumberText;
    private TextView mMeetingPasswordText;

    private InMeetingService mInMeetingService;
    private InMeetingVideoController mInMeetingVideoController;
    private InMeetingAudioController mInMeetingAudioController;
    private Context mContext;

    public interface MeetingOptionBarCallBack {
        void onClickSwitchCamera();

        void onClickLeave();

        void onClickAudio();

        void onClickVideo();

        void onClickChats();

        void onClickAdminBo();

        void onClickReclaimHost();

        void showMoreMenu(PopupWindow popupWindow);

        void onHidden(boolean hidden);
    }


    public MeetingOptionBar(Context context) {
        super(context);
        init(context);
    }

    public MeetingOptionBar(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);

    }

    public MeetingOptionBar(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void setCallBack(MeetingOptionBarCallBack callBack) {
        this.mCallBack = callBack;
    }

    void init(Context context) {
        mContext = context;
        mContentView = LayoutInflater.from(context).inflate(R.layout.layout_meeting_option, this, false);
        addView(mContentView);

        mInMeetingService = ZoomSDK.getInstance().getInMeetingService();
        mInMeetingVideoController = mInMeetingService.getInMeetingVideoController();
        mInMeetingAudioController = mInMeetingService.getInMeetingAudioController();


        mContentView.setOnClickListener(this);
        mBottomBar = findViewById(R.id.bottom_bar);
        mTopBar = findViewById(R.id.top_bar);
        mBtnLeave = findViewById(R.id.btnLeaveZoomMeeting);
        mBtnLeave.setOnClickListener(this);
        mBtnCamera = findViewById(R.id.btnCamera);
        mBtnCamera.setOnClickListener(this);
        mBtnAudio = findViewById(R.id.btnAudio);
        mBtnAudio.setOnClickListener(this);
        findViewById(R.id.btnPlist).setOnClickListener(this);

        mAudioStatusImg = findViewById(R.id.audioStatusImage);
        mVideoStatusImg = findViewById(R.id.videotatusImage);

        findViewById(R.id.moreActionImg).setOnClickListener(this);

        mBtnSwitchCamera = findViewById(R.id.btnSwitchCamera);
        mBtnSwitchCamera.setOnClickListener(this);

        mMeetingNumberText = findViewById(R.id.meetingNumber);
        mMeetingPasswordText = findViewById(R.id.txtPassword);
    }

    Runnable autoHidden = new Runnable() {
        @Override
        public void run() {
            hideOrShowToolbar(true);
        }
    };

    public void hideOrShowToolbar(boolean hidden) {
        removeCallbacks(autoHidden);
        if (hidden) {
            setVisibility(View.INVISIBLE);
        } else {
            postDelayed(autoHidden, 3000);
            setVisibility(View.VISIBLE);
            bringToFront();
        }
        if (null != mCallBack) {
            mCallBack.onHidden(hidden);
        }
    }


    public int getBottomBarHeight() {
        return mBottomBar.getMeasuredHeight();
    }

    public int getBottomBarTop() {
        return mBottomBar.getTop();
    }

    public int getTopBarHeight() {
        return mTopBar.getMeasuredHeight();
    }

    public View getSwitchCameraView() {
        return mBtnSwitchCamera;
    }

    public void updateMeetingNumber(String text) {
        if (null != mMeetingNumberText) {
            mMeetingNumberText.setText(text);
        }
    }

    public void updateMeetingPassword(String text) {
        if (null != mMeetingPasswordText) {
            if (!TextUtils.isEmpty(text)) {
                mMeetingPasswordText.setVisibility(VISIBLE);
                mMeetingPasswordText.setText(text);
            } else {
                mMeetingPasswordText.setVisibility(GONE);
            }
        }
    }

    public void refreshToolbar() {
        updateAudioButton();
        updateVideoButton();
        updateSwitchCameraButton();
    }

    public void updateAudioButton() {
        if (mInMeetingAudioController.isAudioConnected()) {
            if (mInMeetingAudioController.isMyAudioMuted()) {
                mAudioStatusImg.setImageResource(R.drawable.icon_meeting_audio_mute);
            } else {
                mAudioStatusImg.setImageResource(R.drawable.icon_meeting_audio);
            }
        } else {
            mAudioStatusImg.setImageResource(R.drawable.icon_meeting_noaudio);
        }
    }

    public void updateVideoButton() {
        if (mInMeetingVideoController.isMyVideoMuted()) {
            mVideoStatusImg.setImageResource(R.drawable.icon_meeting_video_mute);
        } else {
            mVideoStatusImg.setImageResource(R.drawable.icon_meeting_video);
        }
    }

    public void updateSwitchCameraButton() {
        if (mInMeetingVideoController.isMyVideoMuted()) {
            mBtnSwitchCamera.setVisibility(View.GONE);
        } else {
            mBtnSwitchCamera.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {


        switch (v.getId()) {
            case R.id.btnLeaveZoomMeeting: {
                if (null != mCallBack) {
                    mCallBack.onClickLeave();
                }
                break;
            }
            case R.id.btnCamera: {
                if (null != mCallBack) {
                    mCallBack.onClickVideo();
                }
                break;
            }
            case R.id.btnAudio: {
                if (null != mCallBack) {
                    mCallBack.onClickAudio();
                }
                break;
            }
            case R.id.btnSwitchCamera: {
                if (null != mCallBack) {
                    mCallBack.onClickSwitchCamera();
                }
                break;
            }
            case R.id.moreActionImg: {
                showMoreMenuPopupWindow();
                break;
            }
            case R.id.btnPlist: {
                if (null != mCallBack) {
                    mCallBack.onClickChats();
                }
                break;
            }
            default: {
                setVisibility(INVISIBLE);
                break;
            }
        }

    }

    private boolean isMySelfMeetingHostBoModerator() {
        InMeetingUserInfo myUserInfo = mInMeetingService.getMyUserInfo();
        if (myUserInfo != null && !mInMeetingService.isWebinarMeeting()) {
            InMeetingUserInfo.InMeetingUserRole role = myUserInfo.getInMeetingUserRole();
            return role == InMeetingUserInfo.InMeetingUserRole.USERROLE_HOST ||
                    role == InMeetingUserInfo.InMeetingUserRole.USERROLE_BREAKOUTROOM_MODERATOR;
        }
        return false;
    }

    private void showMoreMenuPopupWindow() {
        final SimpleMenuAdapter menuAdapter = new SimpleMenuAdapter(mContext);
        if (isMySelfMeetingHostBoModerator()) {
            InMeetingBOController boController = mInMeetingService.getInMeetingBOController();
            if (boController.isBOEnabled()) {
                menuAdapter.addItem((new SimpleMenuItem(MENU_CREATE_BO, "Breakout Rooms")));
            }
        }

        if (mInMeetingService.canReclaimHost()) {
            menuAdapter.addItem((new SimpleMenuItem(MENU_RECLAIM_HOST, "Reclaim Host")));
        }

        View popupWindowLayout = LayoutInflater.from(mContext).inflate(R.layout.popupwindow, null);
        ListView shareActions = popupWindowLayout.findViewById(R.id.actionListView);
        final PopupWindow window = new PopupWindow(popupWindowLayout, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        window.setBackgroundDrawable(getResources().getDrawable(R.drawable.bg_transparent));
        shareActions.setAdapter(menuAdapter);
        shareActions.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                SimpleMenuItem item = (SimpleMenuItem) menuAdapter.getItem(position);

                switch (item.getAction()) {
                    case MENU_SHOW_PLIST:
                        if (null != mCallBack) {
                            mCallBack.onClickChats();
                        }
                        break;
                    case MENU_CREATE_BO: {
                        if (null != mCallBack) {
                            mCallBack.onClickAdminBo();
                        }
                        break;
                    }
                    case MENU_RECLAIM_HOST: {
                        if (null != mCallBack) {
                            mCallBack.onClickReclaimHost();
                        }
                        break;
                    }
                }
                window.dismiss();
            }
        });

        window.setFocusable(true);
        window.setOutsideTouchable(true);
        window.update();
        if (null != mCallBack) {
            mCallBack.showMoreMenu(window);
        }
    }

    public boolean isShowing() {
        return getVisibility() == VISIBLE;
    }
}
