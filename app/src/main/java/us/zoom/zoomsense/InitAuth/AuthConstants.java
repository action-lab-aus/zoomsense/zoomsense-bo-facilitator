package us.zoom.zoomsense.InitAuth;

public interface AuthConstants {
	String WEB_DOMAIN = "zoom.us";
	String ZOOM_KEY = "YOUR_ZOOM_KEY";
	String ZOOM_SECRET = "YOUR_ZOOM_SECRET";
}
